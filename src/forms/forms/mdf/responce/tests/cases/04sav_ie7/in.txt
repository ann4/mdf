include ..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet
include ..\..\..\..\in.lib.txt quiet

execute_javascript_stored_lines add_wbt_std_functions
shot_check_png ..\..\shots\00new_ie7.png

execute_javascript_stored_lines fill_mdf_profile_1
wait_click_text "Обновить данными из профиля"

play_stored_lines responce_fields_1

shot_check_png ..\..\shots\01sav_ie7.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\03sav_with_profile.result.xml
exit