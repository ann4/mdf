﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/mdf/responce/e_responce.html'
	, 'forms/base/codec/codec.xml'
	, 'tpl!forms/mdf/responce/v_responce.xaml'
	, 'forms/base/codec/codec.tpl.xaml'
	, 'forms/base/h_times'
	, 'forms/mdf/request/x_request'
	, 'tpl!forms/mdf/responce/v_person.html'
],
function (c_binded, tpl, codec_xml, print_tpl, codec_tpl_xaml, h_times, x_request, v_person)
{
	return function ()
	{
		var controller = c_binded(tpl);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);

			var self = this;
			$(sel + ' input').change(function (e) { self.OnUpdateResolution(); });

			self.UpdatePerson();
			self.OnUpdateResolution();
		}

		controller.UpdatePerson= function()
		{
			if (this.model && this.model.Кому_выдать_документ)
				$(this.binding.form_div_selector + ' div.person').html(v_person(this.model.Кому_выдать_документ));
		}

		controller.SafeUpdateProfileField = function (name, value)
		{
			this.model[name] = value;
			if (this.binding)
				$(this.binding.form_div_selector + ' input[model_field_name="' + name + '"]').val(value);
		}

		controller.UpdateTime = function ()
		{
			if (!this.model)
				this.model = {};
			this.SafeUpdateProfileField('Дата_ответа', h_times.safeDateTime_DD_MM_YYYY());
		}

		controller.UseProfile = function (profile)
		{
			this.UpdateTime();
			if (profile)
			{
				if (profile.Abonent)
				{
					var abonent = profile.Abonent;
					this.model.Учреждение_дающее_разрешение = abonent.Name;
				}
				if (profile.Recipients && profile.Recipients.length > 0 && profile.Recipients[0].Name)
					this.SafeUpdateProfileField('Учреждение_запрашивающее_разрешение', profile.Recipients[0].Name);
				if (profile.ReplyTo)
				{
					var reply_to = profile.ReplyTo;
					var request = x_request().Decode(reply_to);
					if (!this.model.Документ_на_выдачу)
						this.SafeUpdateProfileField('Документ_на_выдачу', request.Документ_на_выдачу);
					if (!this.model.Дата_запроса)
						this.SafeUpdateProfileField('Дата_запроса', request.Дата_запроса);
					if (!this.model.Кому_выдать_документ)
					{
						this.model.Кому_выдать_документ = request.Кому_выдать_документ;
						this.UpdatePerson();
					}
				}
				if (this.binding)
					this.binding.model = this.model;
			}
		}

		controller.OnUpdateResolution= function()
		{
			var sel= this.binding.form_div_selector;
			var UpdateId= function (id)
			{
				var checked = $(sel + ' input#' + id).attr('checked');
				var r = $(sel + ' label[for="' + id + '"]');
				if ('checked' == checked)
				{
					r.addClass('checked');
				}
				else
				{
					r.removeClass('checked');
				}
			}
			UpdateId('cpw-form-mdf-responce-no');
			UpdateId('cpw-form-mdf-responce-yes');
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.BuildXamlView = function ()
		{
			var model = !this.binding ? this.model : base_GetFormContent.call(this);
			var content = print_tpl({
				form: model
			});
			var codec = codec_tpl_xaml();
			return codec.Encode(content);
		}

		var xml_codec = codec_xml();
		xml_codec.schema = { tagName: 'Ответ_на_запрос_разрешения' };
		controller.UseCodec(xml_codec);

		return controller;
	}
});
