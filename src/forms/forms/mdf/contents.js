﻿define
(
	[
		  'forms/collector'

		, 'txt!forms/mdf/request/tests/contents/test1.xml'
		, 'txt!forms/mdf/request/tests/contents/01sav.etalon.xml'

		, 'txt!forms/mdf/responce/tests/contents/test1.xml'
		, 'txt!forms/mdf/responce/tests/contents/01sav.etalon.xml'

		, 'txt!forms/mdf/expense/tests/contents/test1.json.txt'
	],
	function (collect)
	{
		return collect([
		  'request_test1'
		, 'request_01sav'

		, 'responce_test1'
		, 'responce_01sav'

		, 'expense_test1'
		], Array.prototype.slice.call(arguments, 1));
	}
);