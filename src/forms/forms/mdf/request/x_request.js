﻿define([
	  'forms/base/codec/codec.xml'
],
function (codec_xml)
{
	return function ()
	{
		var xml_codec = codec_xml();
		xml_codec.schema = { tagName: 'Запрос_разрешения' };
		return xml_codec;
	}
});
