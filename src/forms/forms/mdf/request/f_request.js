﻿define([
	  'forms/mdf/request/c_request'
	, 'forms/mdf/econtent/c_econtent'
],
function (CreateController, c_econtent)
{
	var file_format = {
		  FilePrefix: 'Request'
		, FileExtension: 'xml'
		, Description: 'МДФ. Запрос разрешения на документ'
	};
	var form_spec =
	{
		CreateController: function () { return c_econtent(CreateController, file_format) }
		, key: 'request'
		, Title: 'Запрос разрешения на выдачу документа'
		, FileFormat: file_format
	};
	return form_spec;
});
