﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/mdf/request/e_request.html'
	, 'forms/base/codec/codec.xml'
	, 'forms/base/h_times'
	, 'tpl!forms/mdf/request/v_request.xaml'
	, 'forms/base/codec/codec.tpl.xaml'
	, 'forms/mdf/request/x_request'
],
function (c_binded, tpl, codec_xml, h_times, print_tpl, codec_tpl_xaml, x_request)
{
	return function ()
	{
		var controller = c_binded(tpl);

		controller.SafeUpdateProfileField= function(name,value)
		{
			this.model[name] = value;
			if (this.binding)
				$(this.binding.form_div_selector + ' input[model_field_name="' + name + '"]').val(value);
		}

		controller.UpdateTime= function()
		{
			if (!this.model)
				this.model = {};
			this.SafeUpdateProfileField('Дата_запроса', h_times.safeDateTime_DD_MM_YYYY());
		}

		controller.UseProfile = function (profile)
		{
			this.UpdateTime();
			if (profile)
			{
				if (profile.Abonent)
				{
					var abonent = profile.Abonent;
					this.model.Учреждение_запрашивающее_разрешение = abonent.Name;
				}
				if (profile.Recipients && profile.Recipients.length>0 && profile.Recipients[0].Name)
					this.SafeUpdateProfileField('Учреждение_дающее_разрешение', profile.Recipients[0].Name);
				if (this.binding)
					this.binding.model = this.model;
			}
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.BuildXamlView = function ()
		{
			var model = !this.binding ? this.model : base_GetFormContent.call(this);
			var content = print_tpl({
				form: model
			});
			var codec = codec_tpl_xaml();
			return codec.Encode(content);
		}

		controller.GetCoveringLetterText = function ()
		{
			return 'Направляем документ\r\n' +
				'   "Запрос разрешения на выдачу документа"';
		}

		controller.GetCoveringLetterAttachmentName = function ()
		{
			return 'Запрос разрешения на документ';
		}

		controller.GetCoveringLetterAttachmentNote = function ()
		{
			return 'Запрос разрешения на выдачу документа';
		}

		controller.UseCodec(x_request());

		return controller;
	}
});
