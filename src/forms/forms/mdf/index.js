﻿define
(
	[
		  'forms/collector'
		, 'forms/mdf/request/c_request'
		, 'forms/mdf/responce/c_responce'
		, 'forms/mdf/expense/c_expense'
	],
	function (collect)
	{
		return collect([
		  'request'
		, 'responce'
		, 'expense'
		], Array.prototype.slice.call(arguments, 1));
	}
);