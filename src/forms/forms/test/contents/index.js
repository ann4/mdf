define
(
	[
		  'contents/collector'
		, 'txt!contents/test/1.txt'
		, 'txt!contents/test/invalid.txt'
	],
	function (collect)
	{
		return collect([
		  '1'
		, 'invalid'
		], Array.prototype.slice.call(arguments,1));
	}
);