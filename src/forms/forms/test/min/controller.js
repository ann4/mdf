define(['forms/base/controller'], function (BaseFormController)
{
	return function ()
	{
		var edit_id_spec = '#test-test-form-edit';
		var res = BaseFormController();

		var RenderForm = function (sel, value)
		{
			var form_div = $(sel);
			var content = '<div style="border-color:green;border-style:solid;border-width:1px;padding:5px;margin:5px;width:500px;">'
			content += 'тестовая форма с одним текстовым полем:';
			content += '<input tyle="edit" id="test-test-form-edit" value="' + value + '"/>';
			content += '</div>';
			form_div.append(content);
		}

		res.CreateNew = function (sel) { RenderForm(sel, ""); };

		res.Edit = function (sel) { RenderForm(sel, this.form_content); };

		res.GetFormContent = function () { return $(edit_id_spec).val(); };

		res.SetFormContent = function (form_content)
		{
			var isnum = /^\d+$/.test(form_content);
			if (!isnum)
			{
				return "Документ должен содержать только цифры!";
			}
			else
			{
				this.form_content = form_content;
				return null;
			}
		};

		res.Validate = function ()
		{
			var form_content = $(edit_id_spec).val();
			var isnum = /^\d+$/.test(form_content);
			return isnum ? null : "Документ должен содержать только цифры!";
		};

		return res;
	}
});
