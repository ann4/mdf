start_store_lines_as add_wbt_std_functions

  app.cpw_Now= function(){return new Date(2015, 6, 26, 17, 36, 0, 0 );};

  wbt_ActionBySpec= function (spec,action)
  {
    try
    {
      var e= $(spec);
      if (!e || null==e)
      {
        return 'can not find element with spec "' + spec + '"';
      }
      else
      {
        var res= action(e);
        return null==res ? null : '"' + spec + '": ' + res;
      }
    }
    catch (ex)
    {
      return 'exception ' + ex.toString();
    }
  }

  wbt_SetValueSimple= function (e,value)
  {
    if (!wbt_CheckMode)
    {
      e.attr('value',value);
      e.change();
      e.trigger('focusout');
      return 'set value "' + value + '"';
    }
    else
    {
      var evalue= e.attr('value');
      if (value==evalue)
      {
        return null; // 'ok, value is "' + value + '"';
      }
      else
      {
        return ' value "' + evalue + '"! it is WRONG!!!!!! should be "' + value + '"';
      }
    }
  }

  wbt_SetValueSelect= function (e,value)
  {
    if (!wbt_CheckMode)
    {
      e.val(value);
      e.change();
      e.trigger('focusout');
      return 'set value "' + value + '"';
    }
    else
    {
      if (('TEXTAREA'==e.prop("tagName")))
      {
		var evalue= e.text();
      }
      else
      {	
        var evalue= e.val();
      }
      if (value==evalue)
      {
        return null; // 'ok, value is "' + value + '"';
      }
      else
      {
        return ' value "' + evalue + '"! it is WRONG!!!!!! should be "' + value + '"';
      }
    }
  }

  wbt_SetValueRadio= function (e,value)
  {
    if (!wbt_CheckMode)
    {
      e.click();
      return 'clicked for value "' + value + '"';
    }
    else
    {
      if (e.attr('checked'))
      {
        return null; // ' ok, value "' + value + '" is checked';
      }
      else
      {
        return ' value "' + value + '" is unchecked! it is WRONG!!!!!!!!';
      }
    }
  }

  wbt_SetValueCheckbox= function (e,value)
  {
    if (!wbt_CheckMode)
    {
      e.attr('checked','undefined'==value ? false : value).change();
      return 'set value "' + value + '"';
    }
    else
    {
      var evalue= e.attr('checked') + '';
      if (value==evalue)
      {
        return null; // ' ok, value "' + value + '" is checked';
      }
      else
      {
        return ' value "' + evalue + '"! it is WRONG!!!!!! should be "' + value + '"';
      }
    }
  }

  wbt_SetValue= function (e,value)
  {
    if (1==e.length)
    {
      return ('SELECT'==e.prop("tagName")) ? wbt_SetValueSelect(e,value) :
             ('checkbox'==e.attr('type')) ?  wbt_SetValueCheckbox(e,value) :
                                             wbt_SetValueSimple(e,value);
    }
    else
    {
      e= e.filter('[value="' + value + '"]');
      if (!e || null==e)
      {
        return 'can not find element for value "' + value + '"';
      }
      else
      {
        return wbt_SetValueRadio(e,value);
      }
    }
  }

  wbt_SetValueBySpec= function (spec,value)
  {
    return wbt_ActionBySpec(spec,function(e){return wbt_SetValue(e,value);});
  }

  wbt_SetModelFieldValue= function (model_field_name,value)
  {
    var spec= '[model_field_name="' + model_field_name + '"]';
    return wbt_SetValueBySpec(spec,value);
  }

  wbt_SetValueById= function (id,value)
  {
    var spec= '#' + id;
    return wbt_SetValueBySpec(spec,value);
  }

  wbt_ClickBySpec= function (spec)
  {
    try
    {
      var e= $(spec);
      if (!e || null==e)
      {
        return 'can not find element with spec "' + spec + '"';
      }
      else
      {
        e.click();
        return 'clicked element for spec "' + spec + '"';
      }
    }
    catch (ex)
    {
      return 'exception ' + ex.toString();
    }
  }

  wbt_ClickByIdValue= function (id, value)
  {
    var spec= '#' + id + '[value=' + value + ']';
    return wbt_ClickBySpec(spec);
  }

  wbt_SetModelRadioValue= function (model_field_name, value)
  {
    var spec= '[model_field_name=' + model_field_name + '][value=' + value + ']';
    return wbt_ClickBySpec(spec);
  }

  wbt_controller_GetFormContentTextArea= function()
  {
    return $('#form-content-text-area').val().replace(new RegExp('\n','g'),'\r\n');
  }

  wbt_async_text_SelectTextInSelect2= function(spec,text,count,func_on_ok)
  {
    var labels= $('.select2-result-label');
    if (labels.length>=1 && -1!=labels.text().indexOf(text))
    {
      func_on_ok(spec);
    }
    else
    {
      if (count>0)
        setTimeout(function(){wbt_async_text_SelectTextInSelect2(spec,text,count-1,func_on_ok);},10);
    }
  }

  wbt_text_TypeTextInSelect2= function(spec,text)
  {
    $(spec + ' .select2-focusser').focus();
    $(spec + ' .select2-choice').click().mousedown().mouseup();
    $(spec + ' .select2-focusser').focus();

    for (var i=0; i<text.length; i++)
    {
      $('.select2-input').val(text.substring(0,i)).change().keydown().keyup();
    }
    $('.select2-input').val(text).change().keydown().keyup();
  }

  wbt_text_SelectTextInSelect2= function(spec,text)
  {
    if (wbt_CheckMode)
    {
      $(spec + ' .select2-focusser').focus();
      $(spec + ' .select2-choice').click().mousedown().mouseup();
      $(spec + ' .select2-focusser').focus();
      $(spec).select2('close');
      return null;
    }
    else
    {
      wbt_text_TypeTextInSelect2(spec,text);
      setTimeout(function(){wbt_async_text_SelectTextInSelect2(spec,text,1000,function(id){
        $('#select2-drop ul li').first().mousedown().mouseup().click();
        $('.select2-input').val('').change().keydown().keyup();
        $(spec).select2('close');
      });},10);
      return 'selection started';
    }
  }

  wbt_Check= function(spec,check)
  {
    var e= $(spec);
    for (var i= 0; i<e.length; i++)
    {
      var ee= $(e[i]);
      if (check(ee))
        return true;
    }
    return false;
  }

  wbt_CheckText= function(spec,text)
  {
    return wbt_Check(spec,function(ee){return text==ee.text();});
  }

  wbt_FocusSelect2= function(sel)
  {
    $(sel + ' .select2-focusser').focus();
    $(sel + ' .select2-choice').click().mousedown().mouseup();
    $(sel + ' .select2-focusser').focus();
    $(sel).select2('close');
  }

  wbt_Select2Focus= function(id)
  {
    $('#s2id_' + id + ' .select2-focusser').focus();
    $('#s2id_' + id + ' .select2-choice').click().mousedown().mouseup();
    $('#s2id_' + id + ' .select2-focusser').focus();
    $('#' + id).select2('close');
  }

  wbt_FocusCloseSelect2BySpec= function(spec)
  {
    var item= $(spec);
    if (1!=item.length)
    {
      return '$("' + spec + '").length=' + item.length;
    }
    else
    {
      var sel= '#s2id_'+item.attr('id');
      wbt_FocusSelect2(sel);
      item.select2('close');
      return '"' + spec + '": focused';
    }
  }

  wbt_SelectTextInSelect2BySpec= function(spec,text)
  {
    var item= $(spec);
    if (1!=item.length)
    {
      return '$("' + spec + '").length=' + item.length;
    }
    else
    {
      var sel= '#s2id_'+item.attr('id');
      if (wbt_CheckMode)
      {
        wbt_FocusSelect2(sel);
        item.select2('close');
        var etext= $(sel + ' .select2-choice span').text();
        return (text==etext) ? null : ' value "' + etext + '"! is WRONG!!!!!! should be "' + text + '"';
      }
      else
      {
        var on_selected_on_text;
        on_selected_on_text= function()
        {
          var labels= $('.select2-result-label');
          item.unbind('select2-loaded',on_selected_on_text);
          if (1>labels.length || -1==$(labels[0]).text().indexOf(text))
          {
            item.select2('close');
            app.wbt_long_process_result= '"' + spec + '": can not find text "' + text + '" to select !!!!!!!!!';
          }
          else
          {
            $('#select2-drop ul li').first().mousedown().mouseup().click();
            $('.select2-input').val('').change().keydown().keyup();
            item.select2('close');
            app.wbt_long_process_result= '"' + spec + '": selected text "' + text + '"';
          }
        };
        var on_selected_on_focus;
        on_selected_on_focus= function()
        {
          item.unbind('select2-loaded',on_selected_on_focus);
          $('.select2-input').val('').change().keydown().keyup().change();
          item.bind('select2-loaded',on_selected_on_text);
          $('.select2-input').val(text).change().keydown().keyup().change();
        };
        item.bind('select2-loaded',on_selected_on_focus);
        wbt_FocusSelect2(sel);
        return "wait_long_process!";
      }
    }
  }

  wbt_CheckIfLongProcessFinished= function()
  {
    if (null==app.wbt_long_process_result)
    {
      return "wait_long_process!";
    }
    else
    {
      var res= app.wbt_long_process_result;
      app.wbt_long_process_result= null
      return res;
    }
  }

  wbt_SelectTextInModelSelect2= function(model_field_name,text)
  {
    var spec= '[model_field_name="' + model_field_name + '"]';
    return wbt_SelectTextInSelect2BySpec(spec,text);
  }

stop_store_lines
